from tatua import db, models

db.create_all()

user1 = models.Provider('sketch', '9340598345', '-1.26434635', '36.90401051','437490vm9vyje5eye', 'plumber')
user2 = models.Provider('lucy', '4353523435', '-1.27298236', '36.9093448', '437490vm9vyje5eye' ,'electrician')
user3 = models.Provider('mike', '435345234', '-1.26350837', '36.91252026', '437490vm9vyje5eye' ,'nanny')
user4 = models.Provider('fred', '45465642', '-1.26123144', '36.9111348', '437490vm9vyje5eye' ,'roofing')
user5 = models.Provider('ras', '2544779867', '-1.27505358', '36.91655707', '437490vm9vyje5eye' ,'carpenter')
user6 = models.Provider('frank', '3243535', '-1.26479672', '36.90823027', '437490vm9vyje5eye' ,'plumber')
user7 = models.Provider('ben', '23423', '-1.26757543', '36.91819788', '437490vm9vyje5eye' ,'electrician')
user8 = models.Provider('brayo', '345345436', '-1.26508302', '36.91660733', '437490vm9vyje5eye' ,'roofing')
user9 = models.Provider('shiko', '32435', '-1.27090734', '36.90516374',  '437490vm9vyje5eye','plumber')
user10 = models.Provider('joy', '5465645', '-1.26811112', '36.9208214','437490vm9vyje5eye' ,'nanny')
user11 = models.Provider('mary', '235436', '-1.26970079', '36.90772166','437490vm9vyje5eye' ,'nanny')

db.session.add(user1)
db.session.add(user2)
db.session.add(user3)
db.session.add(user4)
db.session.add(user5)
db.session.add(user6)
db.session.add(user7)
db.session.add(user8)
db.session.add(user9)
db.session.add(user10)
db.session.add(user11)
# Latitude: 1°15′52″S   -1.26434635
# Longitude: 36°54′14″E   36.90401051
# Distance: 0.9614 km  Bearing: 292.116°
# Latitude: 1°16′23″S   -1.27298236
# Longitude: 36°54′34″E   36.9093448
# Distance: 0.6685 km  Bearing: 206.427°
# Latitude: 1°15′49″S   -1.26350837
# Longitude: 36°54′45″E   36.91252026
# Distance: 0.4585 km  Bearing: 6.965°
# Latitude: 1°15′40″S   -1.26123144
# Longitude: 36°54′40″E   36.9111348
# Distance: 0.7152 km  Bearing: 352.088°
# Latitude: 1°16′30″S   -1.27505358
# Longitude: 36°54′59″E   36.91655707
# Distance: 0.9704 km  Bearing: 148.676°
# Latitude: 1°15′53″S   -1.26479672
# Longitude: 36°54′30″E   36.90823027
# Distance: 0.5243 km  Bearing: 306.501°
# Latitude: 1°16′03″S   -1.26757543
# Longitude: 36°55′06″E   36.91819788
# Distance: 0.687 km  Bearing: 89.768°
# Latitude: 1°15′54″S   -1.26508302
# Longitude: 36°54′59″E   36.91660733
# Distance: 0.5819 km  Bearing: 61.236°
# Latitude: 1°16′15″S   -1.27090734
# Longitude: 36°54′19″E   36.90516374
# Distance: 0.8465 km  Bearing: 244.247°
# Latitude: 1°16′05″S   -1.26811112
# Longitude: 36°55′15″E   36.9208214
# Distance: 0.9803 km  Bearing: 93.321°
# Latitude: 1°16′11″S   -1.26970079
# Longitude: 36°54′28″E   36.90772166
# Distance: 0.532 km  Bearing: 243.954°


cat1 = models.Category('plumber')
cat2 = models.Category('electrician')
cat3 = models.Category('nanny')
cat4 = models.Category('carpenter')
cat5 = models.Category('mover')
cat6 = models.Category('cleaner')
cat7 = models.Category('mechanic')
cat8 = models.Category('chef')
cat9 = models.Category('painter')
cat10 = models.Category('delivery')

db.session.add(cat1)
db.session.add(cat2)
db.session.add(cat3)
db.session.add(cat4)
db.session.add(cat5)
db.session.add(cat6)
db.session.add(cat7)
db.session.add(cat8)
db.session.add(cat9)
db.session.add(cat10)

db.session.commit()
