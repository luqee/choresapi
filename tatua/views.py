import json
import requests
from tatua import app, models, db
from flask import request, abort

@app.route('/tatua/api/v1.0/auth/user/register', methods=['POST'])
def register_user():
    name = request.args.get('username')
    number = request.args.get('number')
    lat = request.args.get('latitude')
    longitude = request.args.get('longitude')
    token = request.args.get('token')
    nuser = models.User(name, number, lat, longitude, token)
    nuser.save()
    mssg = {}
    if nuser.id:
        mssg['result'] = 'success'
        mssg['user_id'] = nuser.id
    else:
        mssg['result'] = 'error'
    return json.dumps(mssg)

@app.route('/tatua/api/v1.0/auth/provider/register', methods=['POST'])
def register_provider():
    name = request.args.get('username')
    number = request.args.get('number')
    lat = request.args.get('latitude')
    longitude = request.args.get('longitude')
    token = request.args.get('token')
    registered_as = request.args.get('registered_as')
    provider = models.Provider(name, number, lat, longitude,token, registered_as)
    provider.save()
    mssg = {}
    if provider.id:
        mssg['result'] = 'success'
        mssg['provider_id'] = provider.id
        mssg['provider_num'] = provider.number
    else:
        mssg['result'] = 'error'
    return json.dumps(mssg)

@app.route('/tatua/api/v1.0/categories', methods=['GET'])
def get_categories():
    categories = models.Category.query.all()
    response = {}
    response['result'] = 'success'
    response['categories'] = []
    for cat in categories:
        cat_item = {}
        cat_item['name'] = cat.name
        cat_item['id'] = cat.id

        response['categories'].append(cat_item)
    
    return json.dumps(response)

@app.route('/tatua/api/v1.0/providers/', methods=['GET'])
def get_providers():
    providers = models.Provider.query.filter_by(registered_as=request.args.get('registered_as')).filter_by(status='Available').all()
    result = {}
    result['providers'] = []
    for plum in providers:
        pro = {}
        pro['id'] = plum.id
        pro['lat'] = plum.latitude
        pro['long'] = plum.longitude

        result['providers'].append(pro)
    return json.dumps(result)

@app.route('/tatua/api/v1.0/users/request', methods=['GET'])
def request_provider():
    if request.method == 'GET':
        user_id = request.args.get('userId')
        prov_cat = request.args.get('provCat')
        selected_provider = models.Provider.query.filter_by(registered_as=prov_cat).filter_by(status='Available').first()
        res = {}
        if selected_provider is not None:
            
            transaction = models.Transaction(user_id,selected_provider.id)
            transaction.save()
            if transaction.id:
                res['message'] = 'success'
                res['activated_user'] = {
                    'provider_name': selected_provider.username,
                    'provider_number': selected_provider.number,
                    'provider_id': selected_provider.id,
                    'transaction_id': transaction.id
                }
                #update provider status to 'Engaged'
                selected_provider.status = 'Engaged'
                db.session.commit()

                user = models.User.query.get(user_id)
                url = 'https://fcm.googleapis.com/fcm/send'
                headers = {
                    'Authorization': 'key='+'AAAAxvIgyUU:APA91bFxvkmWVUQf0pnNrwk8XprlJDqPXNvWUGN72vwEoJO7mt0pFguCfyC8NM3K9QWxnC9ojHJtFHbEoWKyh5vR_RijZjIN7uz7hi56MZTJVLyqBHyZaUhJoelf6hrWqZl5kiZm6xPi',
                    'Content-Type': 'application/json'
                }
                message = {
                    
                    'data': {
                        'message_type': 'user_request',
                        'user_latitude': user.latitude,
                        'user_longitude': user.longitude,
                        'transaction_id': transaction.id
                    },
                    'to': selected_provider.registration_token
                }
                #send push notification to provider
                r = requests.post(url, headers=headers, json=message)

                return json.dumps(res)
        else:
            res['status'] = 'fail'
            return json.dumps(res)
    else:
        return abort(404)

@app.route('/tatua/api/v1.0/auth/provider/changeStatus', methods=['POST'])
def update_status():
    print(request.args.get('status'))
    models.Provider.query.filter_by(id=request.args.get('user_id')).update({
        'status': request.args.get('status')
    })
    db.session.commit()
    result = {}
    result['message'] = 'Success'
    result['current_status'] = request.args.get('status')
    
    return json.dumps(result)

@app.route('/tatua/api/v1.0/auth/provider/submitWork', methods=['POST'])
def submit_work():
    finish_time = request.args.get('f_time')
    descr = request.args.get('desc')
    cost = request.args.get('cost')
    update = models.Transaction.query.filter_by(id=request.args.get('t_id')).update({
        'finish_time': finish_time,
        'service_desription': descr,
        'service_cost': cost
    })
    db.session.commit()
    result = {}
    result['message'] = 'Success'
    
    return json.dumps(result)
